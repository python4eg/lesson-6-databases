import sqlite3 as s

def create_tables(conn):
    conn.execute("""
    create table person(
       id int primary key not null,
       first_name text not null,
       last_name text not null);
    """)

    conn.execute("""
    create table student(
       id int primary key not null,
       first_name text not null,
       last_name text not null,
       telegram_id text,
       final_score int,
       age int not null);
       """)


connection = s.connect('new.db')
data = connection.execute("""
    select * from person;
""")
for row in data:
    print(row)

connection.execute("""
    insert into person values (1, 'Elon', 'Mask');
""")

connection.execute("""
    insert into person values (2, 'Jeff', 'Bezos'), (3, 'Bill', 'Gates');
""")

data = connection.execute("""
    select * from person;
""")
for row in data:
    print(row)
data = connection.execute("""
    select * from person;
""")
print(list(data))

connection.execute("""
insert into student (id, first_name, last_name, age) values (1, 'Певний', 'Студент', 35), (2, 'Непевний', 'Студент', 25);
""")

print('25 Years old')
data = connection.execute('select first_name, last_name, age from student where age = 25;')
for row in data:
    print(row)

print('Older than 25')
data = connection.execute('select first_name, last_name, age from student where age > 25;')
for row in data:
    print(row)

print('Not 25')
data = connection.execute('select first_name, last_name, age from student where age <> 25;')
for row in data:
    print(row)

print('Grow up from 35 to 36')
connection.execute('UPDATE student SET age=36 where age = 35;')
data = connection.execute('select first_name, last_name, age from student where age <> 25;')
for row in data:
    print(row)

print('All age 40')
connection.execute('UPDATE student SET age=40;')
data = connection.execute('select first_name, last_name, age from student where age <> 25;')
for row in data:
    print(row)


print('Extinction')
connection.execute('delete from student where age = 40;')
data = connection.execute('select first_name, last_name, age from student where age <> 25;')
for row in data:
    print(row)

print('Згорів сарай гори і хата')
connection.execute('drop table student;')
data = connection.execute('select first_name, last_name, age from student where age <> 25;')
for row in data:
    print(row)
