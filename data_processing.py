import csv

raw_rows = []
with open('data.csv') as f:
    reader = csv.reader(f)
    for row in reader:
        raw_rows.append(row)

header = raw_rows.pop(0)
list_of_dicts = []
for row in raw_rows:
    list_of_dicts.append(dict(zip(header, row)))
for item in list_of_dicts:
    print(item)


data = []
field_names = []
with open('data.csv') as f:
    reader = csv.DictReader(f)
    field_names = reader.fieldnames
    for row in reader:
        data.append(row)

data.append({'id': '4', 'first_name': 'Richard', 'last_name': 'Branson'})
print(data)
print([row for row in data if row['id'] != '4'])
"""
with open('data.csv', 'w') as f:
    writer = csv.DictWriter(f, field_names)
    writer.writeheader()
    for row in data:
        writer.writerow(row)
"""